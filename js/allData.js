export default   [
    {
      "Year": 1976,
      "Cost": 10.09,

      "moores": 6.061663109
    },
    {
      "Year": 1977,
      "Cost": 8.07,
      "wrghts": 12.27784727,
      "moores": 5.447477725
    },
    {
      "Year": 1978,
      "Cost": 6.2,
      "wrghts": 8.404817848,
      "moores": 4.895523395
    },
    {
      "Year": 1979,
      "Cost": 5.6,
      "wrghts": 5.96488155,
      "moores": 4.399494688
    },
    {
      "Year": 1980,
      "Cost": 5.59,
      "wrghts": 4.59052343,
      "moores": 3.953725056
    },
    {
      "Year": 1981,
      "Cost": 4.96,
      "wrghts": 3.445409209,
      "moores": 3.553122103
    },
    {
      "Year": 1982,
      "Cost": 4.39,
      "wrghts": 2.741470614,
      "moores": 3.193109409
    },
    {
      "Year": 1983,
      "Cost": 3.11,
      "wrghts": 2.223254988,
      "moores": 2.869574252
    },
    {
      "Year": 1984,
      "Cost": 2.67,
      "wrghts": 1.805407113,
      "moores": 2.57882062
    },
    {
      "Year": 1985,
      "Cost": 2.09,
      "wrghts": 1.571085201,
      "moores": 2.317526994
    },
    {
      "Year": 1986,
      "Cost": 1.55,
      "wrghts": 1.417870297,
      "moores": 2.0827084
    },
    {
      "Year": 1987,
      "Cost": 1.17,
      "wrghts": 1.312017947,
      "moores": 1.871682311
    },
    {
      "Year": 1988,
      "Cost": 1.09,
      "wrghts": 1.211299331,
      "moores": 1.682038001
    },
    {
      "Year": 1989,
      "Cost": 1.13,
      "wrghts": 1.112689474,
      "moores": 1.511609005
    },
    {
      "Year": 1990,
      "Cost": 1.12,
      "wrghts": 1.017960904,
      "moores": 1.358448373
    },
    {
      "Year": 1991,
      "Cost": 0.978,
      "wrghts": 0.941858207,
      "moores": 1.220806422
    },
    {
      "Year": 1992,
      "Cost": 0.776,
      "wrghts": 0.873133241,
      "moores": 1.097110755
    },
    {
      "Year": 1993,
      "Cost": 0.646,
      "wrghts": 0.815413546,
      "moores": 0.985948294
    },
    {
      "Year": 1994,
      "Cost": 0.603,
      "wrghts": 0.758354248,
      "moores": 0.886049137
    },
    {
      "Year": 1995,
      "Cost": 0.575,
      "wrghts": 0.706558011,
      "moores": 0.796272055
    },
    {
      "Year": 1996,
      "Cost": 0.544,
      "wrghts": 0.651558419,
      "moores": 0.715591448
    },
    {
      "Year": 1997,
      "Cost": 0.537,
      "wrghts": 0.603782167,
      "moores": 0.643085635
    },
    {
      "Year": 1998,
      "Cost": 0.457,
      "wrghts": 0.55527786,
      "moores": 0.577926323
    },
    {
      "Year": 1999,
      "Cost": 0.412,
      "wrghts": 0.515630531,
      "moores": 0.519369143
    },
    {
      "Year": 2000,
      "Cost": 0.43,
      "wrghts": 0.480092401,
      "moores": 0.466745147
    },
    {
      "Year": 2001,
      "Cost": 0.383,
      "wrghts": 0.44545071,
      "moores": 0.419453167
    },
    {
      "Year": 2002,
      "Cost": 0.342,
      "wrghts": 0.409552566,
      "moores": 0.376952949
    },
    {
      "Year": 2003,
      "Cost": 0.296,
      "wrghts": 0.376591879,
      "moores": 0.338758977
    },
    {
      "Year": 2004,
      "Cost": 0.329,
      "wrghts": 0.354276996,
      "moores": 0.304434929
    },
    {
      "Year": 2005,
      "Cost": 0.336,
      "wrghts": 0.329978454,
      "moores": 0.273588695
    },
    {
      "Year": 2006,
      "Cost": 0.361,
      "wrghts": 0.308499492,
      "moores": 0.24586789
    },
    {
      "Year": 2007,
      "Cost": 0.354,
      "wrghts": 0.28842659,
      "moores": 0.220955838
    },
    {
      "Year": 2008,
      "Cost": 0.35,
      "wrghts": 0.264659674,
      "moores": 0.198567949
    },
    {
      "Year": 2009,
      "Cost": 0.33,
      "wrghts": 0.238557776,
      "moores": 0.178448465
    },
    {
      "Year": 2010,
      "Cost": null,
      "wrghts": null,
      "moores": 0.160367546
    },
    {
      "Year": 2011,
      "Cost": null,
      "wrghts": null,
      "moores": 0.144118639
    },
    {
      "Year": 2012,
      "Cost": null,
      "wrghts": null,
      "moores": 0.129516118
    },
    {
      "Year": 2013,
      "Cost": null,
      "wrghts": null,
      "moores": 0.116393167
    },
    {
      "Year": 2014,
      "Cost": null,
      "wrghts": null,
      "moores": 0.104599872
    },
    {
      "Year": 2015,
      "Cost": null,
      "wrghts": null,
      "moores": 0.094001508
    },
    {
      "Year": 2016,
      "Cost": null,
      "wrghts": null,
      "moores": 0.084477001
    },
    {
      "Year": 2017,
      "Cost": null,
      "wrghts": null,
      "moores": 0.075917545
    },
    {
      "Year": 2018,
      "Cost": null,
      "wrghts": null,
      "moores": 0.068225358
    },
    {
      "Year": 2019,
      "Cost": null,
      "wrghts": null,
      "moores": 0.061312566
    },
    {
      "Year": 2020,
      "Cost": null,
      "wrghts": null,
      "moores": 0.055100198
    },
    {
      "Year": 2021,
      "Cost": null,
      "wrghts": null,
      "moores": 0.049517285
    },
    {
      "Year": 2022,
      "Cost": null,
      "wrghts": null,
      "moores": 0.044500049
    },
    {
      "Year": 2023,
      "Cost": null,
      "wrghts": null,
      "moores": 0.039991174
    },
    {
      "Year": 2024,
      "Cost": null,
      "wrghts": null,
      "moores": 0.035939152
    }
  ]

