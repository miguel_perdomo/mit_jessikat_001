export default {
    "dataCost": [
  {
    "year": 1976,
    "cost": 10.09
  },
  {
    "year": 1977,
    "cost": 8.07
  },
  {
    "year": 1978,
    "cost": 6.2
  },
  {
    "year": 1979,
    "cost": 5.6
  },
  {
    "year": 1980,
    "cost": 5.59
  },
  {
    "year": 1981,
    "cost": 4.96
  },
  {
    "year": 1982,
    "cost": 4.39
  },
  {
    "year": 1983,
    "cost": 3.11
  },
  {
    "year": 1984,
    "cost": 2.67
  },
  {
    "year": 1985,
    "cost": 2.09
  },
  {
    "year": 1986,
    "cost": 1.55
  },
  {
    "year": 1987,
    "cost": 1.17
  },
  {
    "year": 1988,
    "cost": 1.09
  },
  {
    "year": 1989,
    "cost": 1.13
  },
  {
    "year": 1990,
    "cost": 1.12
  },
  {
    "year": 1991,
    "cost": 0.978
  },
  {
    "year": 1992,
    "cost": 0.776
  },
  {
    "year": 1993,
    "cost": 0.646
  },
  {
    "year": 1994,
    "cost": 0.603
  },
  {
    "year": 1995,
    "cost": 0.575
  },
  {
    "year": 1996,
    "cost": 0.544
  },
  {
    "year": 1997,
    "cost": 0.537
  },
  {
    "year": 1998,
    "cost": 0.457
  },
  {
    "year": 1999,
    "cost": 0.412
  },
  {
    "year": 2000,
    "cost": 0.43
  },
  {
    "year": 2001,
    "cost": 0.383
  },
  {
    "year": 2002,
    "cost": 0.342
  },
  {
    "year": 2003,
    "cost": 0.296
  },
  {
    "year": 2004,
    "cost": 0.329
  },
  {
    "year": 2005,
    "cost": 0.336
  },
  {
    "year": 2006,
    "cost": 0.361
  },
  {
    "year": 2007,
    "cost": 0.354
  },
  {
    "year": 2008,
    "cost": 0.35
  },
  {
    "year": 2009,
    "cost": 0.33
  }
],
"dataWrights":[
    
  {
    "year": 1977,
    "wrights": 12.27784727
  },
  {
    "year": 1978,
    "wrights": 8.404817848
  },
  {
    "year": 1979,
    "wrights": 5.96488155
  },
  {
    "year": 1980,
    "wrights": 4.59052343
  },
  {
    "year": 1981,
    "wrights": 3.445409209
  },
  {
    "year": 1982,
    "wrights": 2.741470614
  },
  {
    "year": 1983,
    "wrights": 2.223254988
  },
  {
    "year": 1984,
    "wrights": 1.805407113
  },
  {
    "year": 1985,
    "wrights": 1.571085201
  },
  {
    "year": 1986,
    "wrights": 1.417870297
  },
  {
    "year": 1987,
    "wrights": 1.312017947
  },
  {
    "year": 1988,
    "wrights": 1.211299331
  },
  {
    "year": 1989,
    "wrights": 1.112689474
  },
  {
    "year": 1990,
    "wrights": 1.017960904
  },
  {
    "year": 1991,
    "wrights": 0.941858207
  },
  {
    "year": 1992,
    "wrights": 0.873133241
  },
  {
    "year": 1993,
    "wrights": 0.815413546
  },
  {
    "year": 1994,
    "wrights": 0.758354248
  },
  {
    "year": 1995,
    "wrights": 0.706558011
  },
  {
    "year": 1996,
    "wrights": 0.651558419
  },
  {
    "year": 1997,
    "wrights": 0.603782167
  },
  {
    "year": 1998,
    "wrights": 0.55527786
  },
  {
    "year": 1999,
    "wrights": 0.515630531
  },
  {
    "year": 2000,
    "wrights": 0.480092401
  },
  {
    "year": 2001,
    "wrights": 0.44545071
  },
  {
    "year": 2002,
    "wrights": 0.409552566
  },
  {
    "year": 2003,
    "wrights": 0.376591879
  },
  {
    "year": 2004,
    "wrights": 0.354276996
  },
  {
    "year": 2005,
    "wrights": 0.329978454
  },
  {
    "year": 2006,
    "wrights": 0.308499492
  },
  {
    "year": 2007,
    "wrights": 0.28842659
  },
  {
    "year": 2008,
    "wrights": 0.264659674
  },
  {
    "year": 2009,
    "wrights": 0.238557776
  }

],
"dataMoores":[
     {
    "year": 1976,
    "moores": 6.061663109
  },
  {
    "year": 1977,
    "moores": 5.447477725
  },
  {
    "year": 1978,
    "moores": 4.895523395
  },
  {
    "year": 1979,
    "moores": 4.399494688
  },
  {
    "year": 1980,
    "moores": 3.953725056
  },
  {
    "year": 1981,
    "moores": 3.553122103
  },
  {
    "year": 1982,
    "moores": 3.193109409
  },
  {
    "year": 1983,
    "moores": 2.869574252
  },
  {
    "year": 1984,
    "moores": 2.57882062
  },
  {
    "year": 1985,
    "moores": 2.317526994
  },
  {
    "year": 1986,
    "moores": 2.0827084
  },
  {
    "year": 1987,
    "moores": 1.871682311
  },
  {
    "year": 1988,
    "moores": 1.682038001
  },
  {
    "year": 1989,
    "moores": 1.511609005
  },
  {
    "year": 1990,
    "moores": 1.358448373
  },
  {
    "year": 1991,
    "moores": 1.220806422
  },
  {
    "year": 1992,
    "moores": 1.097110755
  },
  {
    "year": 1993,
    "moores": 0.985948294
  },
  {
    "year": 1994,
    "moores": 0.886049137
  },
  {
    "year": 1995,
    "moores": 0.796272055
  },
  {
    "year": 1996,
    "moores": 0.715591448
  },
  {
    "year": 1997,
    "moores": 0.643085635
  },
  {
    "year": 1998,
    "moores": 0.577926323
  },
  {
    "year": 1999,
    "moores": 0.519369143
  },
  {
    "year": 2000,
    "moores": 0.466745147
  },
  {
    "year": 2001,
    "moores": 0.419453167
  },
  {
    "year": 2002,
    "moores": 0.376952949
  },
  {
    "year": 2003,
    "moores": 0.338758977
  },
  {
    "year": 2004,
    "moores": 0.304434929
  },
  {
    "year": 2005,
    "moores": 0.273588695
  },
  {
    "year": 2006,
    "moores": 0.24586789
  },
  {
    "year": 2007,
    "moores": 0.220955838
  },
  {
    "year": 2008,
    "moores": 0.198567949
  },
  {
    "year": 2009,
    "moores": 0.178448465
  },
  {
    "year": 2010,
    "moores": 0.160367546
  },
  {
    "year": 2011,
    "moores": 0.144118639
  },
  {
    "year": 2012,
    "moores": 0.129516118
  },
  {
    "year": 2013,
    "moores": 0.116393167
  },
  {
    "year": 2014,
    "moores": 0.104599872
  },
  {
    "year": 2015,
    "moores": 0.094001508
  },
  {
    "year": 2016,
    "moores": 0.084477001
  },
  {
    "year": 2017,
    "moores": 0.075917545
  },
  {
    "year": 2018,
    "moores": 0.068225358
  },
  {
    "year": 2019,
    "moores": 0.061312566
  },
  {
    "year": 2020,
    "moores": 0.055100198
  },
  {
    "year": 2021,
    "moores": 0.049517285
  },
  {
    "year": 2022,
    "moores": 0.044500049
  },
  {
    "year": 2023,
    "moores": 0.039991174
  },
  {
    "year": 2024,
    "moores": 0.035939152
  }
]


}