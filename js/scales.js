import data from './allData.js'
    // import { getTotalCasesByDate } from './utils.js'
   
    function totalCasesChart(data, ctx) {
     /* const {
        year,
        cost,
        moores,
        wrigths,
      } = data */
      const chart = new Chart(ctx, {
        type: 'line',
        data: {
          //labels: confirmed.map(item => new Intl.DateTimeFormat('es-MX', { month: 'long', day: 'numeric' }).format(new Date(item.date))),
          labels: data.map(item => item.Year),
          datasets: [
            {
              label: 'Data',
              borderColor: 'red',
              data: data.map(item => item.Cost),
            },
            {
              label: "Wright's law",
              borderColor: 'green',
              data: data.map(item => item.wrghts),
            },
            {
              label: "Moore's law",
              borderColor: 'orange',
              data: data.map(item => item.moores),
            }
          ]
        },
        options: {
          scales: {
            xAxes: [{
              gridLines: {
                display: false,
              }
            }],
            yAxes: [{
              gridLines: {
                display: true,
              },
              //type: 'logarithmic',
            }],
          },
          title: {
            display: true,
            text: 'Photovoltaics (linear)',
            fontSize: 20,
            fontWeigth: 100,
            padding: 30,
            fontColor: '#8d0d35',
          },
          legend: {
            position: 'bottom',
            labels: {
              padding: 20,
              boxWidth: 15,
              fontFamily: 'Montserrat',
              fontColor: 'black',
            }
          },
          layout: {
            padding: {
              right: 20,
              left: 20,
            }
          },
          tooltips: {
            backgroundColor: '#0584f6',
            titleFontSize: 20,
            xPadding: 20,
            yPadding: 20,
            bodyFontSize: 15,
            bodySpacing: 10,
            mode: 'x',
          },
          elements: {
            line: {
              borderWidth: 0,
              fill: false,
            },
            point: {
              radius: 3,
              borderWidth: 2,
              backgroundColor: 'white',
              hoverRadius: 2,
              hoverBorderWidth: 2,
            }
          }
        }
      })
    }
    //async

    function renderCharts() {
      const ctx = document.querySelector('#normalChart').getContext('2d')
      //const data = await getTotalCasesByDate()
      totalCasesChart(data, ctx)
    };
    renderCharts();


    //___________________________________________________________


    import datas from './thisData.js'
  
   
    function totalCasesCharts(datas, ctxs) {
      const {dataMoores, dataWrights, dataCost,} = datas
      const chart = new Chart(ctxs, {
        type: 'line',
        data: {
          //labels: confirmed.map(item => new Intl.DateTimeFormat('es-MX', { month: 'long', day: 'numeric' }).format(new Date(item.date))),
          labels: dataMoores.map(item => item.year),
          datasets: [
            {
              label: 'Data',
              borderColor: 'red',
              data: dataCost.map(item => item.cost),
            },
            {
              label: "Wright's law",
              borderColor: 'green',
              data: dataWrights.map(item => item.wrights),
            },
            {
              label: "Moore's law",
              borderColor: 'orange',
              data: dataMoores.map(item => item.moores),
            }
          ]
        },
        options: {
          scales: {
            xAxes: [{
              gridLines: {
                display: false,
                //color: '#ffffff82',
              },
              ticks: {
                //fontColor: 'white',
              }
              
            }],
            yAxes: [{
              gridLines: {
                display: true,
                //color: '#ffffff82',
              },
              type: 'logarithmic',
              ticks: {
                
                //fontColor: 'blue',
              }
            }],

          },
          title: {
            display: true,
            text: 'Photovoltaics (semi-log)',
            fontSize: 20,
            fontWeigth: 100,
            padding: 30,
            fontColor: '#8d0d35',
          },
          legend: {
            
            position: 'bottom',
            labels: {
              padding: 20,
              boxWidth: 15,
              fontFamily: 'Montserrat',
              fontColor: 'black',
            }
          },
          layout: {
            padding: {
              right: 20,
              left: 20,
            }
            
          },
          tooltips: {
            backgroundColor: '#0584f6',
            titleFontSize: 20,
            xPadding: 20,
            yPadding: 20,
            bodyFontSize: 15,
            bodySpacing: 10,
            mode: 'x',
          },
          elements: {
            line: {
              borderWidth: 0,
              fill: false,
            },
            point: {
              radius: 2,
              borderWidth: 2,
              backgroundColor: 'white',
              hoverRadius: 2,
              hoverBorderWidth: 2,
            }
          }
        }
      })
    }
    //async

    function renderChartss() {
      const ctxs = document.querySelector('#normalCharts').getContext('2d')
      //const data = await getTotalCasesByDate()
      totalCasesCharts(datas, ctxs)
    };
    renderChartss();