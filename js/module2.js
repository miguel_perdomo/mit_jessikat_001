let state = {view: "home"};

function listeners(){
    
    /* esto es para modulo 2 -------------------- */
    document.querySelector('#graphOneBtn').addEventListener('click', viewgraphOne);
    document.querySelector('#graphTwoBtn').addEventListener('click', viewgraphTwo);
    document.querySelector('#graphThreeBtn').addEventListener('click', viewgraphThree);


    

}

window.onload = function(){
    listeners();
}



/* module 2--------------------------------------------------*/

let viewgraphOne = ()=>{
    
    state.view = "image1;"
    document.querySelector("#image1").classList.remove('hidden');
    document.querySelector("#image2").classList.add('hidden');
    document.querySelector("#image3").classList.add('hidden');
    document.querySelector("#graphOneBtn").classList.add('active');
    document.querySelector("#graphTwoBtn").classList.remove('active');
    document.querySelector("#graphThreeBtn").classList.remove('active');


}

let viewgraphTwo = ()=>{

    state.view = "image2;"
    document.querySelector("#image1").classList.add('hidden');
    document.querySelector("#image2").classList.remove('hidden');
    document.querySelector("#image3").classList.add('hidden');
    document.querySelector("#graphOneBtn").classList.remove('active');
    document.querySelector("#graphTwoBtn").classList.add('active');
    document.querySelector("#graphThreeBtn").classList.remove('active');


}

let viewgraphThree = ()=>{

    state.view = "image3;"
    document.querySelector("#image1").classList.add('hidden');
    document.querySelector("#image2").classList.add('hidden');
    document.querySelector("#image3").classList.remove('hidden');
    document.querySelector("#graphOneBtn").classList.remove('active');
    document.querySelector("#graphTwoBtn").classList.remove('active');
    document.querySelector("#graphThreeBtn").classList.add('active');
}