

window.addEventListener("DOMContentLoaded",() => {
    let rageslider1 = new RageSlider({id: "rageslider1"});
  });
  
  class RageSlider {
    constructor(args) {
      let el = document.querySelector(`#${args.id}`),
        isMobile = "ontouchstart" in document.documentElement,
        clientDownEvent = isMobile;
        
      this.track = el.querySelector(".rage__input");
      this.face = el.querySelector(".rage__face");
      this.value = el.querySelector(".rage__value");
      

      window.addEventListener("resize",() => {
        this.adjustCanvas();
      });
     
      this.track.addEventListener("input",() => {
        this.updateFacePos();
      });
  
      window.addEventListener('click',()=>{
        this.refreshFace();
      });
     
      this.updateFacePos();
      
      window.addEventListener('click',()=>{
        this.rangeValue();
      });
     
    }
    
    getRangePercent() {
      let max = this.track.max,
        min = this.track.min,
        relativeValue = this.track.value - min,
        ticks = max - min,
        percent = relativeValue / ticks;
  
      return percent;
    }
    refreshFace(){
      this.face.style.left = `calc(0% - 0em)`;
    }
   
    updateFacePos() {
      let percent = this.getRangePercent(),
        left = percent * 100,
        emAdjust = percent * 1.5;
  
      this.face.style.left = `calc(${left}% - ${emAdjust}em)`;
      this.value.innerHTML = this.track.value;
    }

    rangeValue(){
      let m1 = document.querySelector('#m1');
      let m2 = document.querySelector('#m2');
      let m3 = document.querySelector('#m3');
      let m4 = document.querySelector('#m4');
      let m5 = document.querySelector('#m5');
      let m6 = document.querySelector('#m6');
    
    
      if(this.track.value == 1){
        m1.classList.remove('hidden');
        m2.classList.add('hidden');
        m3.classList.add('hidden');
        m4.classList.add('hidden');
        m5.classList.add('hidden');
        m6.classList.add('hidden');
      }else if(this.track.value == 2){
        m1.classList.add('hidden');
        m2.classList.remove('hidden');
        m3.classList.add('hidden');
        m4.classList.add('hidden');
        m5.classList.add('hidden');
        m6.classList.add('hidden');
      }else if(this.track.value == 3){
        m1.classList.add('hidden');
        m2.classList.add('hidden');
        m3.classList.remove('hidden');
        m4.classList.add('hidden');
        m5.classList.add('hidden');
        m6.classList.add('hidden');
      }else if(this.track.value == 4){
        m1.classList.add('hidden');
        m2.classList.add('hidden');
        m3.classList.add('hidden');
        m4.classList.remove('hidden');
        m5.classList.add('hidden');
        m6.classList.add('hidden');
      }else if(this.track.value == 5){
        m1.classList.add('hidden');
        m2.classList.add('hidden');
        m3.classList.add('hidden');
        m4.classList.add('hidden');
        m5.classList.remove('hidden');
        m6.classList.add('hidden');
      }else if(this.track.value == 6){
        m1.classList.add('hidden');
        m2.classList.add('hidden');
        m3.classList.add('hidden');
        m4.classList.add('hidden');
        m5.classList.add('hidden');
        m6.classList.remove('hidden');
      }else{
        console.log('Error');
      }
    }
    
  }
  
    
  

   
  
  


  
  