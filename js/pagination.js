let state = {view: "home"};

function listeners(){
    document.querySelector('#btnM1').addEventListener('click', viewMat1);
    document.querySelector('#btnM2').addEventListener('click', viewMat2);
    document.querySelector('#btnM3').addEventListener('click', viewMat3);
    document.querySelector('#btnM4').addEventListener('click', viewMat4);
    document.querySelector('#btnM5').addEventListener('click', viewMat5);
    document.querySelector('#btnM6').addEventListener('click', viewMat6);

}
window.onload = function(){
    listeners();
}


let viewMat1 = ()=>{

    state.view = "btnM1;"
    document.querySelector('#m1').classList.remove('hidden');
    document.querySelector('#m2').classList.add('hidden');
    document.querySelector('#m3').classList.add('hidden');
    document.querySelector('#m4').classList.add('hidden');
    document.querySelector('#m5').classList.add('hidden');
    document.querySelector('#m6').classList.add('hidden');
    
    document.querySelector('#btnM1').classList.add('active');
    document.querySelector('#btnM2').classList.remove('active');
    document.querySelector('#btnM3').classList.remove('active');
    document.querySelector('#btnM4').classList.remove('active');
    document.querySelector('#btnM5').classList.remove('active');
    document.querySelector('#btnM6').classList.remove('active');

}
let viewMat2 = ()=>{

    state.view = "btnM2;"
    document.querySelector('#m1').classList.add('hidden');
    document.querySelector('#m2').classList.remove('hidden');
    document.querySelector('#m3').classList.add('hidden');
    document.querySelector('#m4').classList.add('hidden');
    document.querySelector('#m5').classList.add('hidden');
    document.querySelector('#m6').classList.add('hidden');
    
    document.querySelector('#btnM1').classList.remove('active');
    document.querySelector('#btnM2').classList.add('active');
    document.querySelector('#btnM3').classList.remove('active');
    document.querySelector('#btnM4').classList.remove('active');
    document.querySelector('#btnM5').classList.remove('active');
    document.querySelector('#btnM6').classList.remove('active');

}
let viewMat3 = ()=>{

    state.view = "btnM3;"
    document.querySelector('#m1').classList.add('hidden');
    document.querySelector('#m2').classList.add('hidden');
    document.querySelector('#m3').classList.remove('hidden');
    document.querySelector('#m4').classList.add('hidden');
    document.querySelector('#m5').classList.add('hidden');
    document.querySelector('#m6').classList.add('hidden');

    document.querySelector('#btnM1').classList.remove('active');
    document.querySelector('#btnM2').classList.remove('active');
    document.querySelector('#btnM3').classList.add('active');
    document.querySelector('#btnM4').classList.remove('active');
    document.querySelector('#btnM5').classList.remove('active');
    document.querySelector('#btnM6').classList.remove('active');

}
let viewMat4 = ()=>{

    state.view = "btnM4;"
    document.querySelector('#m1').classList.add('hidden');
    document.querySelector('#m2').classList.add('hidden');
    document.querySelector('#m3').classList.add('hidden');
    document.querySelector('#m4').classList.remove('hidden');
    document.querySelector('#m5').classList.add('hidden');
    document.querySelector('#m6').classList.add('hidden');

    document.querySelector('#btnM1').classList.remove('active');
    document.querySelector('#btnM2').classList.remove('active');
    document.querySelector('#btnM3').classList.remove('active');
    document.querySelector('#btnM4').classList.add('active');
    document.querySelector('#btnM5').classList.remove('active');
    document.querySelector('#btnM6').classList.remove('active');

}
let viewMat5 = ()=>{

    state.view = "btnM5;"
    document.querySelector('#m1').classList.add('hidden');
    document.querySelector('#m2').classList.add('hidden');
    document.querySelector('#m3').classList.add('hidden');
    document.querySelector('#m4').classList.add('hidden');
    document.querySelector('#m5').classList.remove('hidden');
    document.querySelector('#m6').classList.add('hidden');

    document.querySelector('#btnM1').classList.remove('active');
    document.querySelector('#btnM2').classList.remove('active');
    document.querySelector('#btnM3').classList.remove('active');
    document.querySelector('#btnM4').classList.remove('active');
    document.querySelector('#btnM5').classList.add('active');
    document.querySelector('#btnM6').classList.remove('active');

}
let viewMat6 = ()=>{

    state.view = "btnM6;"
    document.querySelector('#m1').classList.add('hidden');
    document.querySelector('#m2').classList.add('hidden');
    document.querySelector('#m3').classList.add('hidden');
    document.querySelector('#m4').classList.add('hidden');
    document.querySelector('#m5').classList.add('hidden');
    document.querySelector('#m6').classList.remove('hidden');

    document.querySelector('#btnM1').classList.remove('active');
    document.querySelector('#btnM2').classList.remove('active');
    document.querySelector('#btnM3').classList.remove('active');
    document.querySelector('#btnM4').classList.remove('active');
    document.querySelector('#btnM5').classList.remove('active');
    document.querySelector('#btnM6').classList.add('active');

}
